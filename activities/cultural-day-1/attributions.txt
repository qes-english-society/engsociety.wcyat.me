Attributions for images used

slide 1: ．怪貓．, CC BY 2.0 <https://creativecommons.org/licenses/by/2.0>, via Wikimedia Commons
slide 2: Mcy jerry at en.wikipedia., CC BY-SA 3.0 <http://creativecommons.org/licenses/by-sa/3.0/>, via Wikimedia Commons
slide 7: User:Xchen27, CC BY-SA 3.0 <https://creativecommons.org/licenses/by-sa/3.0>, via Wikimedia Commons
slide 8: Chad Miller, CC BY-SA 2.0 <https://creativecommons.org/licenses/by-sa/2.0>, via Wikimedia Commons
slide 10: User:Gisling, CC BY-SA 3.0 <http://creativecommons.org/licenses/by-sa/3.0/>, via Wikimedia Commons
slide 14: Wefk423, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons
slide 17: Guilhem Vellut from Paris, France, CC BY 2.0 <https://creativecommons.org/licenses/by/2.0>, via Wikimedia Commons
slide 19: Zhu Jian / Flickr user: zhu4905jian ( https://www.flickr.com/photos/7672101@N06/ ), CC BY 2.0 <https://creativecommons.org/licenses/by/2.0>, via Wikimedia Commons
thank you: Ashashyou, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons
