const classes = [
  "1A",
  "1B",
  "1C",
  "1D",
  "2A",
  "2B",
  "2C",
  "2D",
  "3A",
  "3B",
  "3C",
  "3D",
  "4A",
  "4B",
  "4C",
  "4D",
  "5A",
  "5B",
  "5C",
  "5D",
  "6A",
  "6B",
  "6C",
  "6D",
  "1a",
  "1b",
  "1c",
  "1d",
  "2a",
  "2b",
  "2c",
  "2d",
  "3a",
  "3b",
  "3c",
  "3d",
  "4a",
  "4b",
  "4c",
  "4d",
  "5a",
  "5b",
  "5c",
  "5d",
  "6a",
  "6b",
  "6c",
  "6d",
];
let submithtml;
function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function thankyou() {
  document.getElementById("mainform").innerHTML =
    '<h4 style="color: #212529;font-size: 60px;">Thank you!</h4><br>/n<button class="redirect" onclick="redirect()"><p class="buttontext">Return to main page</p></button>';
  await sleep(8000);
  document.querySelector(".pageclip-form__success").click();
}

function redirect() {
  window.location.href = "/";
}

function init() {
  submithtml = document.getElementById("submit").innerHTML;
  document.getElementById("submit").innerHTML =
    '<input type="button" class="btn btn-primary submit-now" onclick="check()" value="Submit">';
}

function check() {
  let validclass = false;
  const email = document.getElementById("email");
  email.select();
  const classinput = document.getElementById("class");
  classinput.select();
  const classnum = document.getElementById("classnum");
  classnum.select();
  for (let i = 0; i < classes.length; i++) {
    if (classinput.value === classes[i]) {
      validclass = true;
    }
  }
  if (
    email.value.endsWith("@cloud.qes.edu.hk") &&
    validclass &&
    classnum.value < 40 &&
    classnum.value > 0 &&
    classnum.value.length < 3
  ) {
    document.getElementById("submit").innerHTML = submithtml;
    document.getElementById("submitbtn").click();
  } else {
    alert(
      "Please fill in the form properly! (have you entered your school email, class and class number correctly? Do not include any spaces.)"
    );
  }
}

$(document).on("keypress", function (e) {
  if (e.which === 13) {
    check();
  }
});

const submitbtn = document.getElementById("mainform");
submitbtn.addEventListener("submit", thankyou);
